<?php
	ini_set( 'display_errors' , E_ALL ) ;

	$urlhost = 'http://rashodnika.net/' ;
	$temp_dir = implode( DIRECTORY_SEPARATOR , [ sys_get_temp_dir( ) , sha1( 'rashodnika.net' ) ] ) ;
	@mkdir( $temp_dir ) ;

	$dbh = new \PDO( 'mysql:localhost;dbname=cartridge' , 'root' , 'f2ox9erm' ) ;
	$dbh->exec( 'USE `cartridge`' ) ;
	$dbh->exec( 'SET names utf8mb4' ) ;
	$dbh->setAttribute( \PDO::ATTR_ERRMODE , \PDO::ERRMODE_WARNING ) ;

	$dbh->exec( "
CREATE TABLE IF NOT EXISTS `cartridge_category`(
	`id` BIGINT( 22 ) UNSIGNED NOT null AUTO_INCREMENT COMMENT 'идентификатор' ,
	`id_parent` BIGINT( 22 ) UNSIGNED NOT null COMMENT 'родительская категория' ,
	`url` VARCHAR( 100 ) CHARACTER SET utf8mb4 NOT null COMMENT 'URL' ,
	`title` TEXT CHARACTER SET utf8mb4 DEFAULT null COMMENT 'доп.название' ,
	`name` VARCHAR( 100 ) CHARACTER SET utf8mb4 NOT null COMMENT 'название' ,
	`level` TINYINT( 1 ) UNSIGNED NOT null COMMENT 'уровень вложенности категории (1 - брэнд, 4 - модель устройства, 6 - модель расходника)' ,

	PRIMARY KEY( `id` ) ,
	UNIQUE( `url` , `level` ) ,
	INDEX( `id_parent` ) ,
	INDEX( `url` , `id_parent` , `name` ) USING BTREE ,
	INDEX( `level` ) ,
	INDEX( `name` , `id_parent` , `url` , `level` ) USING BTREE
)
ENGINE = InnoDB
DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_unicode_ci
COMMENT 'Категории товара' ;
	" ) ;

	$dbh->exec( "
CREATE TABLE IF NOT EXISTS `cartridge_category_alt`(
	`category_id` BIGINT( 22 ) UNSIGNED NOT null COMMENT 'расходник (level = 6)' ,
	`category_id_alt` BIGINT( 22 ) UNSIGNED NOT null COMMENT 'устройство (level = 4)' ,

	PRIMARY KEY ( `category_id` , `category_id_alt` ) ,
	UNIQUE( `category_id_alt` , `category_id` ) USING BTREE
)
ENGINE = InnoDB
DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_unicode_ci
COMMENT 'Расходник-модель принтера' ;
	" ) ;

	$dbh->exec( "
CREATE TABLE IF NOT EXISTS `cartridge_category_info`(
	`category_id` BIGINT( 22 ) UNSIGNED NOT null COMMENT 'идентификатор расходника' ,
	`key` VARCHAR( 500 ) NOT null COMMENT 'ключ доп.информации' ,
	`value` LONGTEXT COMMENT 'значение доп.информации' ,
	`data_type` TINYINT( 1 ) UNSIGNED NOT null DEFAULT 1 COMMENT '1 - текст, 2 - картинка' ,

	INDEX( `category_id` , `data_type` )
)
ENGINE = InnoDB
DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_unicode_ci
COMMENT 'Доп.информация к категории' ;
	" ) ;

	$dbh->exec( "
CREATE OR REPLACE VIEW `v_cartridge_product` AS
SELECT
	`c5`.`id` AS `category_id` ,
	concat_ws( ' ' , `c5`.`title` , `c3`.`name` , `c4`.`name` , `c5`.`name` ) AS `title`
FROM
	`cartridge_category` AS `c1`

	INNER JOIN `cartridge_category` AS `c2` ON
	( `c1`.`id` = `c2`.`id_parent` )

	INNER JOIN `cartridge_category` AS `c3` ON
	( `c2`.`id` = `c3`.`id_parent` )

	INNER JOIN `cartridge_category` AS `c4` ON
	( `c3`.`id` = `c4`.`id_parent` )

	INNER JOIN `cartridge_category` AS `c5` ON
	( `c4`.`id` = `c5`.`id_parent` )
WHERE
	( `c1`.`id_parent` = 0 ) ;
	" ) ;

	$dbh->exec( "
CREATE OR REPLACE VIEW `v_cartridge_product_info` AS
SELECT
	`c1`.`id` AS `category_id` ,
	`ci1`.`key` ,
	`ci1`.`value`
FROM
	`cartridge_category` AS `c1`

	INNER JOIN `cartridge_category_info` AS `ci1` ON
	( `c1`.`id` = `ci1`.`category_id` )
WHERE
	( `c1`.`level` = 6 ) AND
	( `ci1`.`data_type` = 1 ) ;
	" ) ;

	$dbh->exec( "
CREATE OR REPLACE VIEW `v_cartridge_product_info2` AS
SELECT 
	`cci1`.`key` AS `key`,
	`cci1`.`value` AS `value`,
	`ci1`.`name` AS `mark`,
	`ci6`.`name` AS `brand`,
	`cci1`.`data_type` AS `data_type`
FROM
	`cartridge_category_info` AS `cci1`

	INNER JOIN `cartridge_category` AS `ci1` ON
	(`cci1`.`category_id` = `ci1`.`id`)

	INNER JOIN `cartridge_category` AS `ci2` ON
	(`ci1`.`id_parent` = `ci2`.`id`)

	INNER JOIN `cartridge_category` AS `ci3` ON
	(`ci2`.`id_parent` = `ci3`.`id`)

	INNER JOIN `cartridge_category` AS `ci4` ON
	(`ci3`.`id_parent` = `ci4`.`id`)

	INNER JOIN `cartridge_category` AS `ci5` ON
	(`ci4`.`id_parent` = `ci5`.`id`)

	INNER JOIN `cartridge_category` AS `ci6` ON
	(`ci5`.`id_parent` = `ci6`.`id`) ;
	" ) ;

	$dbh->exec( "
CREATE OR REPLACE VIEW `v_cartridge_product_image` AS
SELECT
	`c1`.`id` AS `category_id` ,
	`ci1`.`value` AS `href`
FROM
	`cartridge_category` AS `c1`

	INNER JOIN `cartridge_category_info` AS `ci1` ON
	( `c1`.`id` = `ci1`.`category_id` )
WHERE
	( `c1`.`level` = 6 ) AND
	( `ci1`.`data_type` = 2 ) ;
	" ) ;

	$dbh->exec( "
CREATE OR REPLACE VIEW `v_cartridge_product_alt` AS
SELECT
	`ca1`.`category_id` AS `product_id`,
	`ca1`.`category_id_alt` AS `product_id_alt`,
	`cc16`.`name` AS `product_brand`,
	`cc11`.`title` AS `product_title`,
	`cc11`.`name` AS `product_model`,
	`cc25`.`name` AS `product_brand_alt`,
	`cc22`.`name` AS `product_model_alt`,
	`cc21`.`name` AS `product_submodel_alt`
FROM
	`cartridge_category_alt` AS `ca1`

        INNER JOIN `cartridge_category` AS `cc11` ON
	(`ca1`.`category_id` = `cc11`.`id`)

        INNER JOIN `cartridge_category` AS `cc12` ON
	(`cc11`.`id_parent` = `cc12`.`id`)

        INNER JOIN `cartridge_category` AS `cc13` ON
	(`cc12`.`id_parent` = `cc13`.`id`)

        INNER JOIN `cartridge_category` AS `cc14` ON
	(`cc13`.`id_parent` = `cc14`.`id`)

        INNER JOIN `cartridge_category` AS `cc15` ON
	(`cc14`.`id_parent` = `cc15`.`id`)

        INNER JOIN `cartridge_category` AS `cc16` ON
	(`cc15`.`id_parent` = `cc16`.`id`)

        INNER JOIN `cartridge_category` AS `cc21` ON
	(`ca1`.`category_id_alt` = `cc21`.`id`)

        INNER JOIN `cartridge_category` AS `cc22` ON
	(`cc21`.`id_parent` = `cc22`.`id`)

        INNER JOIN `cartridge_category` AS `cc23` ON
	(`cc22`.`id_parent` = `cc23`.`id`)

        INNER JOIN `cartridge_category` AS `cc24` ON
	(`cc23`.`id_parent` = `cc24`.`id`)

        INNER JOIN `cartridge_category` AS `cc25` ON
	(`cc24`.`id_parent` = `cc25`.`id`) ;
	" ) ;

	$sth_select_level = $dbh->prepare( '
SELECT
	`c1`.*
FROM
	`cartridge_category` AS `c1`
WHERE
	( `c1`.`level` = :level ) ;
	' ,[
		\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => false ,
	] ) ;

	$sth_select_level2 = $dbh->prepare( '
SELECT DISTINCT
	`cc1`.*
FROM
	`cartridge_category` AS `cc1`

	LEFT OUTER JOIN `cartridge_category_info` AS `cci1` ON
	( `cc1`.`id` = `cci1`.`category_id` )
WHERE
	( `cci1`.`category_id` IS null ) AND
	( `cc1`.`level` = :level ) ;
	' ,[
		\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => false ,
	] ) ;

	$sth_delete_info = $dbh->prepare( '
DELETE
	`ci1`.*
FROM
	`cartridge_category_info` AS `ci1`
WHERE
	( `ci1`.`category_id` = :category_id ) ;
	' ,[
		\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => false ,
	] ) ;

	$sth_delete_alt = $dbh->prepare( '
DELETE
	`ca1`.*
FROM
	`cartridge_category_alt` AS `ca1`
WHERE
	( `ca1`.`category_id` = :category_id ) ;
	' ,[
		\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => false ,
	] ) ;

	$sth_insert_info = $dbh->prepare( '
INSERT INTO
	`cartridge_category_info`
SET
	`category_id` := :category_id ,
	`key` := :key ,
	`value` := :value ,
	`data_type` := :data_type ;
	' ,[
		\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => false ,
	] ) ;

	$sth_insert_alt = $dbh->prepare( '
INSERT INTO
	`cartridge_category_alt`
SET
	`category_id` := :category_id ,
	`category_id_alt` := :category_id_alt ;
	' ,[
		\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => false ,
	] ) ;

	$sth_insert = $dbh->prepare( '
INSERT INTO
	`cartridge_category`
SET
	`url` := :url ,
	`id_parent` := :id_parent ,
	`name` := :name ,
	`title` := :title ,
	`level` := :level
ON DUPLICATE KEY UPDATE
	`url` := :url ,
	`id_parent` := :id_parent ,
	`title` := :title ,
	`level` := :level
	' ,[
		\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => false ,
	] ) ;

	$sth_select = $dbh->prepare( '
SELECT
	`c1`.`id`
FROM
	`cartridge_category` AS `c1`
WHERE
	( `c1`.`url` = :url ) AND
	( `c1`.`level` = :level )
LIMIT 1 ;
	' ,[
		\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => false ,
	] ) ;

	function get_categories( ... $level ) {
		global $dbh ;

		$levels = implode( ',' , array_map( function( $item ) use( $dbh ) {
			return $dbh->quote( $item ) ;
		} , $level ) ) ;

		$sth = $dbh->prepare( '
SELECT
	`c1`.*
FROM
	`cartridge_category` AS `c1`
WHERE
	( `c1`.`level` IN ( ' . $levels . ' ) ) ;
		' ,[
			\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => false ,
		] ) ;
		$sth->execute( ) ;

		while ( $category = $sth->fetch( \PDO::FETCH_ASSOC ) ) {
			yield $category ;
		}

		$sth->closeCursor( ) ;
	}

	function get_category( $url , $level = 5 ) {
		global $sth_select ;

		$sth_select->bindValue( ':url' , $url , \PDO::PARAM_STR ) ;
		$sth_select->bindValue( ':level' , $level , \PDO::PARAM_INT ) ;
		$sth_select->execute( ) ;

		list( $id ) = $sth_select->fetch( \PDO::FETCH_NUM ) ;

		$sth_select->closeCursor( ) ;

		return $id ;
	}

	function add_category( $url , $name , $level , $id_parent = 0 , $title = null ) {
		global $sth_insert ;

		$sth_insert->bindValue( ':id_parent' , $id_parent , \PDO::PARAM_INT ) ;
		$sth_insert->bindValue( ':url' , $url , \PDO::PARAM_STR ) ;
		$sth_insert->bindValue( ':name' , $name , \PDO::PARAM_STR ) ;
		$sth_insert->bindValue( ':title' , $title , \PDO::PARAM_STR ) ;
		$sth_insert->bindValue( ':level' , $level , \PDO::PARAM_INT ) ;
		$sth_insert->execute( ) ;
		$sth_insert->closeCursor( ) ;

		return get_category( $url , $level ) ;
	}

	function add_category_alt( $category_id , $category_alt_url ) {
		global $sth_insert_alt ;

		if ( $category_id_alt = get_category( $category_alt_url ) ) {
			$sth_insert_alt->bindValue( ':category_id' , $category_id , \PDO::PARAM_INT ) ;
			$sth_insert_alt->bindValue( ':category_id_alt' , $category_id_alt , \PDO::PARAM_INT ) ;
			$sth_insert_alt->execute( ) ;
			$sth_insert_alt->closeCursor( ) ;
		}

		return $category_id_alt ;
	}

	function add_category_info( $category_id , $key , $value , $data_type = 1 ) {
		global $sth_insert_info ;

		$key = trim( $key ) ;

		if ( empty( $key ) ) {
			return null ;
		}

		$value = trim( $value ) ;

		$sth_insert_info->bindValue( ':category_id' , $category_id , \PDO::PARAM_INT ) ;
		$sth_insert_info->bindValue( ':key' , $key , \PDO::PARAM_STR ) ;
		$sth_insert_info->bindValue( ':value' , $value , \PDO::PARAM_STR ) ;
		$sth_insert_info->bindValue( ':data_type' , $data_type , \PDO::PARAM_INT ) ;
		$sth_insert_info->execute( ) ;
		$sth_insert_info->closeCursor( ) ;

		return true ;
	}

	function del_category_info( $category_id ) {
		global $sth_delete_info ;

		$sth_delete_info->bindValue( ':category_id' , $category_id , \PDO::PARAM_INT ) ;
		$sth_delete_info->execute( ) ;
		$sth_delete_info->closeCursor( ) ;

		return true ;
	}

	function del_category_alt( $category_id ) {
		global $sth_delete_alt ;

		$sth_delete_alt->bindValue( ':category_id' , $category_id , \PDO::PARAM_STR ) ;
		$sth_delete_alt->execute( ) ;
		$sth_delete_alt->closeCursor( ) ;

		return true ;
	}

	function getURL( $url ) {
		global $temp_dir ;

		$file_name = implode( DIRECTORY_SEPARATOR , [ $temp_dir , sha1( $url ) ] ) ;

		if ( ! file_exists( $file_name ) ) {
			$data = file_get_contents( $url ) ;
			file_put_contents( $file_name , $data ) ;

			return $data ;
		}

		echo $file_name . PHP_EOL ;

		return file_get_contents( $file_name ) ;
	}

	function document( $url ) {
		$domh = new DOMDocument( ) ;
		@$domh->loadHTML( '<?xml version="1.0" encoding="utf-8"?>' . getURL( $url ) ) ;

		return new DOMXPath( $domh ) ;
	}

	$brand_list = [ ] ;

	foreach ( document( $urlhost )->query( '//div[@id="sections"]/ul/li/a' ) as $brand_item ) {
		$brand_item_href = $urlhost . $brand_item->getAttribute( 'href' ) ;
		$brand_item_name = $brand_item->textContent ;
		$brand_list[ $brand_item_name ] = [
			'url' => $brand_item_href ,
			'item' => [ ] ,
		] ;
		$brand_item_id = add_category( $brand_item_href , $brand_item_name , 1 ) ;

		$brand_item_doch = document( $brand_item_href ) ;

		foreach ( $brand_item_doch->query( '//div[@id="sections"]/ul/li[span[@class="selected"]]/ul/li/a' ) as $brand_item_category ) {
			$brand_item_category_href = $urlhost . $brand_item_category->getAttribute( 'href' ) ;
			$brand_item_category_name = $brand_item_category->textContent ;
			$brand_list[ $brand_item_name ][ 'item' ][ $brand_item_category_name ] = [
				'url' => $brand_item_category_href ,
				'item' => [ ] ,
			] ;

			$brand_item_category_id = add_category( $brand_item_category_href , $brand_item_category_name , 2 , $brand_item_id ) ;

			foreach ( $brand_item_doch->query( '../ul/li/a' , $brand_item_category ) as $brand_item_subcat ) {
				$brand_item_subcat_href = $urlhost . $brand_item_subcat->getAttribute( 'href' ) ;
				$brand_item_subcat_name = $brand_item_subcat->textContent ;

				$brand_list[ $brand_item_name ][ 'item' ][ $brand_item_category_name ][ 'item' ][ $brand_item_subcat_name ] = [
					'url' => $brand_item_subcat_href ,
					'item' => [
					] ,
				] ;

				$brand_item_subcat_id = add_category( $brand_item_subcat_href , $brand_item_subcat_name , 3 , $brand_item_category_id ) ;
				$brand_item_subcat_doch = document( $brand_item_subcat_href ) ;

				foreach ( $brand_item_subcat_doch->query( '//div[@id="menu3"]/h4' ) as $brand_item_subcat2 ) {
					$brand_item_subcat2_name = $brand_item_subcat2->textContent ;
					$node = null ;

					while ( $brand_item_subcat2 = $brand_item_subcat2->nextSibling ) {
						$brand_item_subcat2_name_original = $brand_item_subcat2->textContent ;
						$brand_item_subcat2_name = preg_replace( '{\W+}s' , ' ' , $brand_item_subcat2_name_original ) ;
						$brand_item_subcat2_name = trim( $brand_item_subcat2_name ) ;

						switch ( strToLower( @$brand_item_subcat2->tagName ) ) {
							case 'h4' : {
								$brand_item_subcat4_id = add_category(
									$brand_item_subcat_href . '#' .  urlencode( $brand_item_subcat2_name_original ) ,
									$brand_item_subcat2_name , 4 , $brand_item_subcat_id
								) ;

								$node = &$brand_list[ $brand_item_name ][ 'item' ][ $brand_item_category_name ]
									[ 'item' ][ $brand_item_subcat_name ][ 'item' ][ $brand_item_subcat2_name ] ;

								break ;
							}
							case 'a' : {
								$brand_item_subcat2_href = $urlhost . $brand_item_subcat2->getAttribute( 'href' ) ;
								$brand_item_subcat2_name = preg_replace( '{\[(.+?)\]}' , '\1' , $brand_item_subcat2_name ) ;

								$node[ 'item' ][ $brand_item_subcat2_name_original ] = [
									'url' => $brand_item_subcat2_href ,
									'item' => [ ] ,
								] ;

								$brand_item_subcat3_id = add_category( $brand_item_subcat2_href , $brand_item_subcat2_name , 5 ,
									$brand_item_subcat4_id ) ;
								$brand_item_subcat3_doch = document( $brand_item_subcat2_href ) ;

								foreach ( $brand_item_subcat3_doch->query( '//div[@id="listoftovar"]//table[1]//tr[position( )>1]/td[2]' ) as $brand_item_subcat3 ) {
									$brand_item_subcat3_node = $brand_item_subcat3->getElementsByTagName( 'a' )->item( 0 ) ;
									$brand_item_subcat4_href = $urlhost . $brand_item_subcat3_node->getAttribute( 'href' ) ;
									$brand_item_subcat4_title = $brand_item_subcat3_node->getAttribute( 'title' ) ;
									$brand_item_subcat4_name = $brand_item_subcat3_node->textContent ;

									$node[ 'item' ][ $brand_item_subcat2_name ][ 'item' ][ $brand_item_subcat4_name ] = [
										'url' => $brand_item_subcat4_href ,
										'title' => $brand_item_subcat4_title ,
									] ;

									$brand_item_subcat5_id = add_category(
										$brand_item_subcat4_href , $brand_item_subcat4_name , 6 ,
										$brand_item_subcat3_id , $brand_item_subcat4_title
									) ;

									if ( $brand_item_subcat3->getAttribute( 'colspan' ) > 0 ) {
										continue ;
									}

									foreach ( $brand_item_subcat3_doch->query( 'following-sibling::td[1]' , $brand_item_subcat3 ) as $brand_item_subcat6 ) {
										$brand_item_subcat6_node = $brand_item_subcat6->getElementsByTagName( 'a' )->item( 0 ) ;
										$brand_item_subcat6_name = $brand_item_subcat6_node->textContent ;
										$brand_item_subcat6_href = $brand_item_subcat6_node->getAttribute( 'href' ) . '#' . $brand_item_subcat6_name ;
										$brand_item_subcat6_title = null ;

										foreach ( $brand_item_subcat3_doch->query( 'following-sibling::td[1]//a/text( )' , $brand_item_subcat6 ) as $brand_item_subcat6_title_node ) {
											$brand_item_subcat6_title = $brand_item_subcat6_title_node->textContent ;

											break ;
										}

										$brand_item_subcat6_id = add_category( $brand_item_subcat6_href , $brand_item_subcat6_name , 7 ,
											$brand_item_subcat5_id , $brand_item_subcat6_title ) ;

										break ;
									}
								}

								break ;
							}
						}
					}

					break ;
				}
			}
		}
	}

	foreach ( get_categories( 6 , 7 ) as $category ) {
		$doch = document( $category[ 'url' ] ) ;

		foreach ( $doch->query( '//*[class="listoftovar"]/following-sibling::table//img/@src' ) as $src ) {
			add_category_info( $category[ 'id' ] , 'image' , $src->textContent ) ;
		}

		del_category_info( $category[ 'id' ] ) ;
		del_category_alt( $category[ 'id' ] ) ;

		foreach ( $doch->query( '//div[@class="slider-box"]//img/@src' ) as $image_src ) {
			add_category_info( $category[ 'id' ] , 'image' , $urlhost . $image_src->textContent , 2 ) ;
		}

		foreach ( $doch->query( '//div[@id="resultat"]/following-sibling::ul//li//a/@href' ) as $alt_href ) {
			add_category_alt( $category[ 'id' ] , $urlhost . $alt_href->textContent ) ;
		}

		foreach ( $doch->query( '//div[@id="resultat"]/following-sibling::table[ 1 ]//tr' ) as $sibling_href ) {
			$tds = $doch->query( './/td' , $sibling_href ) ;
			$value = $doch->document->saveHTML( $tds->item( 1 ) ) ;
			$value = preg_replace( '{^<td.*?>(.*?)</td>$}is' , '$1' , $value ) ;

			add_category_info( $category[ 'id' ] , $tds->item( 0 )->textContent , $value ) ;
		}
	}